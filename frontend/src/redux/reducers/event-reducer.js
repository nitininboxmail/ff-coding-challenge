import {ActionName} from "../actions/event-action";

let initialState = {
    events : [],
    currPage : 1,
    totalPages : 1,
    filter : {},
    categories: []
};

export const eventReducer = (state = initialState, action) => {
    switch (action.type){
        case ActionName.SET_EVENTS:{
            return {...state , ...action.payload};
        }

        case ActionName.SET_PAGE:{
            return {...state , ...action.payload};
        }

        case ActionName.SET_FILTERS:{
            return {...state , ...action.payload}
        }

        case ActionName.SET_CATEGORIES:
            return {...state, ...action.payload};

        default:
            return state
    }
}