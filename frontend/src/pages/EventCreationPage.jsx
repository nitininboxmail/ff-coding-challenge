import React, {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import EventService from '../services/event-service';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useNavigate} from 'react-router-dom';
import TimeZonePicker from '../components/TimeZonePicker';


export default function EventCreationPage() {

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [category, setCategory] = useState("")
    const [address, setAddress] = useState("")
    const [isVirtual, setIsVirtual] = useState("")
    const [date, setDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [selectedTimezone, setSelectedTimezone] = useState({})
    const [conferenceDetails, setConferenceDetails] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [videoUrl, setVideoUrl] = useState("");

    const navigate = useNavigate();

    const handleAddEvent = async () => {
        EventService.createEvent({
            title,
            description,
            category,
            date,
            isVirtual,
            address,
            endDate,
            selectedTimezone,
            conferenceDetails,
            imageUrl,
            videoUrl
        })
            .then((res) => {
                navigate('/');
                toast.success('Successfuly added new event!!!', {position: toast.POSITION.TOP_CENTER});
            })
            .catch((e) =>
                toast.error(e.response.data.error.message, {position: toast.POSITION.TOP_CENTER})
            )
    }

    return (
        <div className='add-event-container'>
            <div className='form'>
                <Form>
                    <Form.Group className="mb-1" controlId="exampleForm.ControlInput1">
                        <Form.Label style={{fontSize: "25px", fontWeight: "600"}}>New Event</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-1" controlId="exampleForm.ControlInput1">
                        <Form.Label>Title<span style={{color: "red"}}>*</span></Form.Label>
                        <Form.Control required type="text" placeholder="Enter event title"
                                      onChange={(e) => setTitle(e.target.value)} defaultValue={title}/>
                    </Form.Group>
                    <Form.Group className="mb-1" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Description<span style={{color: "red"}}>*</span></Form.Label>
                        <Form.Control as="textarea" style={{resize: "none"}} rows={2}
                                      placeholder="Enter event description"
                                      onChange={(e) => setDescription(e.target.value)} defaultValue={description}/>
                    </Form.Group>
                    <Form.Group className="mb-1" controlId="exampleForm.ControlInput1">
                        <Form.Label>Category<span style={{color: "red"}}>*</span></Form.Label>
                        <Form.Control type="text" placeholder="Enter event category"
                                      onChange={(e) => setCategory(e.target.value)} defaultValue={category}/>
                    </Form.Group>
                    <Form.Group className="mb-1" style={{display: "flex"}}>
                        <Form.Group style={{width: "50%"}}>
                            <Form.Label>Event mode<span style={{color: "red"}}>*</span></Form.Label>
                            <Form.Select aria-label="Default select example"
                                         onChange={(e) => setIsVirtual(e.target.value)} defaultValue={isVirtual}>
                                <option value={""}>Select event mode</option>
                                <option value={true}>Virtual</option>
                                <option value={false}>Offline</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group style={{width: "50%", marginLeft: "1rem"}}>
                            <Form.Label>Select timezone<span
                                style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span></Form.Label>
                            <TimeZonePicker
                                selectedTimezone={selectedTimezone} setSelectedTimezone={setSelectedTimezone}
                            />
                        </Form.Group>
                    </Form.Group>


                    <Form.Group className="mb-1" style={{display: "flex"}}>
                        <Form.Group style={{width: "50%"}}>
                            <Form.Label>Event start date<span style={{color: "red"}}>*</span></Form.Label>
                            <Form.Control type="date" onChange={(e) => setDate(e.target.value)} defaultValue={date}/>
                        </Form.Group>
                        <Form.Group style={{width: "50%", marginLeft: "1rem"}}>
                            <Form.Label>Event end date<span
                                style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span></Form.Label>
                            <Form.Control type="date"
                                          onChange={(e) => setEndDate(e.target.value)} defaultValue={endDate}
                            />
                        </Form.Group>
                    </Form.Group>

                    <Form.Group className="mb-1" controlId="exampleForm.ControlTextarea1">
                        <Form.Label style={{marginTop: ".5rem"}}>Address
                            {
                                isVirtual === "true" ?
                                    <span style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span>
                                    : <span style={{color: "red"}}>*</span>
                            }
                        </Form.Label>
                        <Form.Control as="textarea" style={{resize: "none"}} rows={2} placeholder="Enter event address"
                                      onChange={(e) => setAddress(e.target.value)} defaultValue={address}/>
                    </Form.Group>

                    <Form.Group className="mb-1" controlId="exampleForm.ControlTextarea1">
                        <Form.Label style={{marginTop: ".5rem"}}>Video conference details
                            {
                                isVirtual === "false" ?
                                    <span style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span>
                                    : <span style={{color: "red"}}>*</span>
                            }
                        </Form.Label>
                        <Form.Control as="textarea" style={{resize: "none"}} rows={2}
                                      placeholder="Example:- Zoom meeting details"
                                      onChange={(e) => setConferenceDetails(e.target.value)}
                                      defaultValue={conferenceDetails}
                        />
                    </Form.Group>

                    <Form.Group className="mb-4" style={{display: "flex"}}>
                        <Form.Group style={{width: "50%"}}>
                            <Form.Label>Cover image / Thumbnail<span
                                style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span></Form.Label>
                            <Form.Control type="url" placeholder="Paste image url"
                                          onChange={(e) => setImageUrl(e.target.value)} defaultValue={imageUrl}
                            />
                        </Form.Group>
                        <Form.Group style={{width: "50%", marginLeft: "1rem"}}>
                            <Form.Label>YouTube / Video<span
                                style={{fontSize: "14px", fontFamily: "sans-seri"}}> (optional)</span></Form.Label>
                            <Form.Control type="url" placeholder="Paste video url"
                                          onChange={(e) => setVideoUrl(e.target.value)} defaultValue={videoUrl}
                            />
                        </Form.Group>
                    </Form.Group>


                    <Form.Group className="mb-2" controlId="exampleForm.ControlTextarea1">
                        <Button variant="success" style={{width: "100%"}} onClick={handleAddEvent}>Add Event</Button>
                    </Form.Group>
                </Form>
            </div>
        </div>
    )
}
