import React, {useEffect} from 'react'
import EventList from '../components/EventList'
import EventFilters from '../components/EventFilters'
import {fetchCategoriesAction} from "../redux/actions/event-action";
import {useDispatch} from "react-redux";

export default function HomePage() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCategoriesAction());
    }, [])

    return (
        <div className='app-container'>
            <div className='actions-container'>
                <EventFilters/>
            </div>
            <div className='divider'/>
            <div className='events-container'>
                <div className='heading-container'>
                    <div className='heading'>
                        Events
                    </div>
                </div>
                <EventList/>
            </div>
        </div>
    )
}
