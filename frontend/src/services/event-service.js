import axios from "axios"

const api_key = process.env.REACT_APP_API_KEY
const API_BASE_URL = process.env.REACT_APP_BASE_URL


export default class EventService {

    static config = {
        headers: { api_key }
    }

    static createEvent = ({ title, description, category, date, isVirtual, address, endDate, selectedTimezone, conferenceDetails, imageUrl, videoUrl }) => {
        return axios.post(`${API_BASE_URL}`, {
            title, description, category, date, isVirtual, address, endDate, selectedTimezone, conferenceDetails, imageUrl, videoUrl
        }, this.config)
    }

    static fetchEvents = (params) => {
        const newParams = Object.keys(params)
            .filter(key => !(params[key] === undefined || params[key] === ""))
            .reduce((obj, key) => {
                obj[key] = params[key];
                return obj;
            }, {})

        let queryString = new URLSearchParams(newParams).toString();
        return axios.get(`${API_BASE_URL}?${queryString}`, this.config)
    }

    static fetchCategories = () => {
        return axios.get(`${API_BASE_URL}/category`, this.config)
    }
}