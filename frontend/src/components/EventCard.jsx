import React from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { BsCalendarDay } from "react-icons/bs";
import { MdOutlineCategory } from "react-icons/md";
import { ImLocation } from "react-icons/im";
import {AiOutlineFieldTime ,AiOutlineYoutube} from "react-icons/ai";
import {BiDetail} from "react-icons/bi"

export default function EventCard({ event }) {
  const date = new Date(event.date).toDateString();
  const endDate = new Date(event.endDate).toDateString();


  let dateText = "";
  const eventStartDate = new Date(event.date);
  const eventEndDate = new Date(event.endDate);
  const currDate = new Date();
  const diffTime = Math.abs(currDate - eventStartDate);
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  const dayText = diffDays == 1 ?  " day" : " days"
  if(eventStartDate > currDate) dateText = " (After " + diffDays + dayText + ")";
  else if(eventStartDate < currDate) dateText = " (Ended " + diffDays + dayText + " ago)"
  else if(eventStartDate <= currDate && eventEndDate >= currDate) dateText = " (Ongoing)"


  return (
    <Card style={{ width: '80%', margin: "1rem", height: "fit-content", boxShadow: "0px 0px 19px -6px blue" }}>
      <Card.Img variant="top"
        src={event.imageUrl ? event.imageUrl : "https://www.messagetech.com/wp-content/themes/ml_mti/images/no-image.jpg"}
        style={{ width: "100%", height: "10rem", objectFit: "fill" }} />
      <Card.Body>
        <Card.Title style={{display:"flex",alignItems:"center"}}>{event.title}<span style={{fontSize:"14px",marginLeft:"10px"}}>{dateText}</span></Card.Title>
        <Card.Text>
          {event.description}
        </Card.Text>
      </Card.Body>
      <ListGroup className="list-group-flush">
        {
          event.selectedTimezone ?
            <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><AiOutlineFieldTime style={{ marginRight: ".5rem" }} /><span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>TimeZone &rarr;</span>{event.selectedTimezone.label}</ListGroup.Item>
            : ""
        }
        <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><BsCalendarDay style={{ marginRight: ".5rem" }} /><span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>Start date &rarr;</span>{date}</ListGroup.Item>
        {
          event.endDate ?
            <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><BsCalendarDay style={{ marginRight: ".5rem" }} /><span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>End date &rarr;</span>{endDate}</ListGroup.Item>
            : ""
        }
        <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><MdOutlineCategory style={{ marginRight: ".5rem" }} /><span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>Category &rarr;</span>{event.category}</ListGroup.Item>

        <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><ImLocation style={{ marginRight: ".5rem" }} />
        
        <span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>Address &rarr;</span>
        {event.address}{event.isVirtual ? "   (Virtual)" : ""}</ListGroup.Item>

        {
          event.conferenceDetails ?
            <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><BiDetail style={{ marginRight: ".5rem" }} /><span style={{ fontSize: "16px", fontWeight: "600", marginRight: "1rem" }}>Conference details &rarr;</span>{event.conferenceDetails}</ListGroup.Item>
            :""
        }

        {
          event.videoUrl ?
          <ListGroup.Item style={{ display: "flex", alignItems: "center" }}><AiOutlineYoutube style={{ marginRight: ".5rem" }} /><a href={event.videoUrl}>Click to view event video</a></ListGroup.Item>
          :""
        }

      </ListGroup>
    </Card>
  )
}
