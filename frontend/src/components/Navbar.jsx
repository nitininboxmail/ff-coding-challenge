import {useState} from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/esm/Button';
import Navbar from 'react-bootstrap/Navbar';
import {useLocation, useNavigate} from 'react-router-dom';

function NavBar() {
  const navigate = useNavigate();
  const {pathname} = useLocation();

  const handleClick = () => {
    if(pathname !== "/event")
    {
      navigate('/event');
    }
    else
    {
      navigate('/');
    }
  }

  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand style={{cursor:"pointer"}} onClick={()=>{
            navigate('/')
          }}>
            <img
              alt=""
              src="../../logo192.png"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{' '}
            HOME
          </Navbar.Brand>
          <Navbar.Collapse className="justify-content-end">
            <Button variant="dark" size="lg" style={{ color:"white"}} onClick={()=>{
              handleClick();
            }}>{pathname !== "/event" ?"Add new event" : "Go Back"}</Button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;