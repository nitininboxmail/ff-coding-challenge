import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Pagination} from "react-bootstrap";
import {setPageAction} from "../redux/actions/event-action";

export default function PaginationComp() {
    const {totalPages, currPage} = useSelector(state => state.event);
    const dispatch = useDispatch();

    const pageItems = [];
    for (let i = 1; i <= totalPages; i++) {
        pageItems.push(
            <Pagination.Item key={i} onClick={() => dispatch(setPageAction(i))} active={i === currPage}>
                {i}
            </Pagination.Item>,
        );
    }
    return (
        <div className='full-width'>
            <Pagination>{pageItems}</Pagination>
        </div>
    )
}
