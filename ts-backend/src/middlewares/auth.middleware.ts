import {NextFunction, Request, Response} from 'express';
import {HttpException} from '@exceptions/HttpException';
import { AUTH_KEY } from '@/config';

const authMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const ApiKey = req.cookies['api_key'] || req.header('api_key') ;
    if (ApiKey === AUTH_KEY)
    next()
  } catch (error) {
    next(new HttpException(401, 'Wrong authentication token'));
  }
};

export default authMiddleware;
