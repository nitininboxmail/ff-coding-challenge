
import { RequestHandler } from 'express';
import { HttpException } from '@exceptions/HttpException';
import {ObjectSchema} from "joi";
import {EventSchema} from "@/joi-validation/event.validation";

export const validationMiddleware = (
  JoiSchema: ObjectSchema,
  key: string | 'body' | 'query' | 'params' = 'body',
  skipMissingProperties = false,
  whitelist = true,
  forbidNonWhitelisted = true,
): RequestHandler => {
  return (req, res, next) => {
    const obj = req[key];
    const {value, error } = JoiSchema.validate(obj);
    if (error){
      res.status(400).send({error: {message: error.message}});
    }else {
      req[key] = value;
      next();
    }
  };
};
