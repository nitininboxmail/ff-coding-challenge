import * as Joi from "joi";


export interface IEvent {
  title: string,
  description: string,
  category: string,
  date: string,
  isVirtual: boolean,
  address: string
}

export interface IFilter{
  title?: string,
  category?: string,
  isVirtual?: boolean,
  address?: string,
  currPage: number
}
