import {config} from 'dotenv';

config({path: `.env.${process.env.NODE_ENV || 'development'}.local`});

export const CREDENTIALS = process.env.CREDENTIALS === 'true';
export const EVENTS_PER_PAGE = 5;
export const {
  NODE_ENV, PORT, SECRET_KEY, LOG_FORMAT, LOG_DIR, ORIGIN, DB_CONNECTION,
  AUTH_KEY
} = process.env;
