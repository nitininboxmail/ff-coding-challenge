import {IEvent, IFilter} from '@interfaces/events.interface';
import {EVENTS_PER_PAGE} from "@config";
import {EventModel} from "@models/events.model";

class EventService {

  public async findAll(filterData: IFilter): Promise<any> {
    let {currPage, ...filters} = filterData;
    const newFilters = Object.keys(filters)
      .filter(key => !(filters[key] === undefined || filters[key] === ""))
      .reduce((obj: any, key) => {
        obj[key] = (key === "isVirtual" ? filters[key] : new RegExp(filters[key], "i"))
        return obj;
      }, {});
    const skipCount = (currPage - 1) * EVENTS_PER_PAGE;
    const eventModels = await EventModel.find(newFilters).limit(EVENTS_PER_PAGE).skip(skipCount);
    const events = eventModels.map(event => event._doc);
    const totalEventCount = await EventModel.find(newFilters).count()
    const totalPages = Math.ceil(totalEventCount / EVENTS_PER_PAGE);
    return {events, skipCount, totalPages}
  }

  public async createEvent(eventData: IEvent): Promise<any> {
    const event = await EventModel.create(eventData);
    return event;
  }

  public async findAllCategories(): Promise<any> {
    const categories = await EventModel.distinct("category")
    return categories;
  }
}

export default EventService;
